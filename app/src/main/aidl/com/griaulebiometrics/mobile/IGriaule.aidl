package com.griaulebiometrics.mobile;

// Declare the interface.
interface IGriaule {
	String connect();
	String capture();
}