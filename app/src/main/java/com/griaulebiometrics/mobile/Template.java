package com.griaulebiometrics.mobile;

public class Template {

    private byte[] buffer;

    private int quality;

    public Template(byte[] buffer, int quality) {
        super();
        this.buffer = buffer;
        this.quality = quality;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public void setBuffer(byte[] buffer) {
        this.buffer = buffer;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

}
