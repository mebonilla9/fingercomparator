package com.griaulebiometrics.mobile.sample;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.griaulebiometrics.mobile.util.GrConstants;

import co.gov.icfes.fingercomparator.actividades.FingerActivity;

public class IncomingHandler extends Handler {

    private FingerActivity activity = null;

    public IncomingHandler(FingerActivity activity) {
        super();
        this.activity = activity;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case GrConstants.IMAGE_ACQURIED:
                Bundle bundle = msg.getData();
                activity.setImage(bundle.getByteArray("buffer"), bundle.getByteArray("image"));
                break;
        }
        super.handleMessage(msg);
    }

}
