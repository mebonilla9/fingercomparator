package com.griaulebiometrics.mobile;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import co.gov.icfes.fingercomparator.persistencia.entidades.Autorizados;

public class NativeService {

    private static final int GR_MAX_TPT_SIZE = 10000;

    public native static int letsMatch(byte[] queryTpt, byte[] refTpt, int qSize[], int rSize[]);

    public native static int letsExtract(byte[] rawImg, int width, int height, int xRes, int yRes, byte tpt[], int tptSize[]);

    private static NativeService instance;

    public static int MATCH_THREASHOLD = 60;

    public static NativeService getInstance() {
        if (instance == null) {
            instance = new NativeService();
        }
        return instance;
    }

    public NativeService() {
        System.loadLibrary("griaule");
    }

    public byte[] extraerTemplate(byte[] imagen) {
        Template template = null;
        try {
            byte[] temBase = new byte[GR_MAX_TPT_SIZE];
            int[] temSize = new int[1];
            temSize[0] = GR_MAX_TPT_SIZE;

            int calidad = letsExtract(imagen.clone(), 320, 480, 500, 500, temBase, temSize);

            Log.e("Calidad del template", "La calidad del template es del: " + calidad + " %");

            return temBase.clone();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public Template extractTemplate(byte[] image) {
        byte[] template = new byte[GR_MAX_TPT_SIZE];
        int[] tptSize = new int[1];
        tptSize[0] = GR_MAX_TPT_SIZE;

        int quality = letsExtract(image.clone(), 320, 480, 500, 500, template, tptSize);

        File tptFile = new File(Environment.getExternalStorageDirectory(), "template003.tpt");
        FileOutputStream tptOut = null;
        try {
            tptOut = new FileOutputStream(tptFile.getAbsolutePath());
            tptOut.write(template.clone());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (tptOut != null) {
                    tptOut.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("com.griaulebiometrics.mobile", "wrote template003.tpt");

        return new Template(template.clone(), quality);
    }

    public int compararHuellasExaminado(byte[] templateConsulta, byte[] templateReferencia) {
        int[] queryTemplateSize = new int[1];
        queryTemplateSize[0] = GR_MAX_TPT_SIZE;
        int[] referenceTemplateSize = new int[1];
        referenceTemplateSize[0] = templateReferencia.length;

        int puntaje = letsMatch(templateConsulta.clone(), templateReferencia.clone(), queryTemplateSize, referenceTemplateSize);
        Log.d("Comparacion huella", "Match score: " + puntaje);
        if (puntaje >= NativeService.MATCH_THREASHOLD) {
            return puntaje;
        }
        return 0;
    }

    public Autorizados compararHuellaLogin(byte[] templateConsulta, List<Autorizados> listaAutorizados) {
        try {
            int[] queryTemplateSize = new int[1];
            queryTemplateSize[0] = GR_MAX_TPT_SIZE;
            for(Autorizados aut : listaAutorizados){
                int[] referenceTemplateSize = new int[1];
                referenceTemplateSize[0] = aut.getTemplate().length;
                int puntaje = letsMatch(templateConsulta.clone(), aut.getTemplate().clone(), queryTemplateSize, referenceTemplateSize);
                Log.d("Comparacion huella", "Match score: " + puntaje);
                if (puntaje >= NativeService.MATCH_THREASHOLD) {
                    return aut;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SearchResult matchTemplate(byte[] queryTemplate, int index, String pguid, byte[] referenceTemplate) {

        int[] queryTemplateSize = new int[1];
        queryTemplateSize[0] = GR_MAX_TPT_SIZE;
        int[] referenceTemplateSize = new int[1];
        referenceTemplateSize[0] = referenceTemplate.length;

        Log.d("com.griaulebiometrics.mobile", "Match");
        int score = letsMatch(queryTemplate.clone(), referenceTemplate.clone(), queryTemplateSize, referenceTemplateSize);
        Log.d("com.griaulebiometrics.mobile", "Match score: " + score);

        return new SearchResult(pguid, index, score);
    }

    public SearchResult verify(Finger finger, Profile profile) {
        for (Finger profileFinger : profile.getFingers()) {
            SearchResult searchResult = NativeService.getInstance().matchTemplate(finger.getTemplate(), profileFinger.getIndex(), profile.getGuid(), profileFinger.getTemplate());
            if (searchResult.getScore() >= NativeService.MATCH_THREASHOLD && (finger.getIndex() == null || !finger.getIndex().equals(profileFinger.getIndex()))) {
                searchResult.setName(profile.getName());
                return searchResult;
            }
        }
        return null;
    }

    public SearchResult identify(byte[] template, List<Profile> profiles) {
        for (Profile profile : profiles) {
            if (!profile.getEnrollMode().equals(EnrollMode.LOCAL)) {
                continue;
            }
            for (Finger finger : profile.getFingers()) {
                SearchResult searchResult = NativeService.getInstance().matchTemplate(template, finger.getIndex(), profile.getGuid(), finger.getTemplate());
                if (searchResult.getScore() >= NativeService.MATCH_THREASHOLD) {
                    searchResult.setName(profile.getName());
                    return searchResult;
                }
            }
        }
        return null;
    }

}
