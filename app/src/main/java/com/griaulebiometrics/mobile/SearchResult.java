package com.griaulebiometrics.mobile;

/**
 * Class representing a search result from GBS Server
 *
 * @author rodrigo.giolo
 */
public class SearchResult {

    private String pguid;

    private Integer index;

    private Integer score;

    private String name;

    /**
     * Construtor from search result, with indicates pguid, fingerprint index and score from server cloud search
     *
     * @param pguid
     * @param index
     * @param score
     */
    public SearchResult(String pguid, Integer index, Integer score) {
        super();
        this.pguid = pguid;
        this.index = index;
        this.score = score;
    }

    @Override
    public String toString() {
        return "SearchResult [pguid=" + pguid + ", index=" + index + ", score=" + score + ", name=" + name + "]";
    }

    /**
     * Get pguid property
     *
     * @return pguid
     */
    public String getPguid() {
        return pguid;
    }

    /**
     * Set pguid property
     *
     * @param pguid
     */
    public void setPguid(String pguid) {
        this.pguid = pguid;
    }

    /**
     * Get fingerprint index property
     *
     * @return index
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * Set fingerprint index property
     *
     * @param index
     */
    public void setIndex(Integer index) {
        this.index = index;
    }

    /**
     * Get search score property
     *
     * @return score
     */
    public Integer getScore() {
        return score;
    }

    /**
     * Set search score property
     *
     * @param score
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
