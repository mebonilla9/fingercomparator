package com.griaulebiometrics.mobile;


public class Finger implements Comparable<Finger> {

    private Integer index;

    private byte[] template;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + index;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Finger other = (Finger) obj;
        if (index != other.index)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Finger [index=" + index + ", template=" + (template == null ? "null" : template.length) + "]";
    }

    public Finger(Integer index, byte[] template) {
        super();
        this.index = index;
        this.template = template;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public byte[] getTemplate() {
        return template;
    }

    public void setTemplate(byte[] template) {
        this.template = template;
    }

    @Override
    public int compareTo(Finger another) {
        return this.index < another.index ? -1 : this.index > another.index ? 1 : 0;
    }

}
