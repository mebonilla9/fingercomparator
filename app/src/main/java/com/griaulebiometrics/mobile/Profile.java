package com.griaulebiometrics.mobile;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Profile {

    public static final String NOT_FROM_ANDROID_SAMPLE = "Person is not from Android Sample";

    private String guid;

    private String name;

    private List<Finger> fingers;

    private EnrollMode enrollMode;
    ;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((guid == null) ? 0 : guid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Profile other = (Profile) obj;
        if (guid == null) {
            if (other.guid != null)
                return false;
        } else if (!guid.equals(other.guid))
            return false;
        return true;
    }


    @Override
    public String toString() {
        return name + " (" + fingers.size() + " finger" + (fingers.size() > 1 ? "s" : "") + ")";
    }

    public Profile() {
        this.guid = null;
        this.name = null;
        this.fingers = new LinkedList<Finger>();
        this.enrollMode = null;
    }

    public Profile(String guid, String name, byte[] template, EnrollMode enrollMode) {
        super();
        this.guid = guid;
        this.name = name;
        this.fingers = new LinkedList<Finger>();
        this.setFinger(new Finger(0, template));
        this.enrollMode = enrollMode;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Finger> getFingers() {
        return fingers;
    }

    public void setFingers(List<Finger> fingers) {
        this.fingers = fingers;
    }


    public EnrollMode getEnrollMode() {
        return enrollMode;
    }

    public void setEnrollMode(EnrollMode enrollMode) {
        this.enrollMode = enrollMode;
    }

    public Finger getFinger(int i) {
        for (Finger f : this.fingers) {
            if (f.getIndex() == i) {
                return f;
            }
        }
        return null;
    }

    public Finger getFirstFinger() {
        return this.fingers.get(0);
    }

    public void setFinger(Finger finger) {
        boolean found = false;
        for (Finger f : this.fingers) {
            if (f.equals(finger)) {
                this.fingers.set(this.fingers.indexOf(f), finger);
                found = true;
                break;
            }
        }
        if (!found) {
            this.fingers.add(finger);
        }
        Collections.sort(this.fingers);
    }

    public void removeFinger(int index) {
        Finger finger = null;
        for (Finger f : this.fingers) {
            if (f.getIndex() == index) {
                finger = f;
                break;
            }
        }
        if (finger != null) {
            this.fingers.remove(finger);
        }
        Collections.sort(this.fingers);
    }

    public String serializeMetadata() {
        return "from Sample Android;name:" + this.name + ";fingers:" + this.fingers.size();
    }

    public static String getNameFromMetadata(String metadata) {
        if (metadata == null || metadata.equals("")) {
            return Profile.NOT_FROM_ANDROID_SAMPLE;
        }
        String[] fields = metadata.split(";");
        if (fields.length < 3 || !fields[0].equals("from Sample Android")) {
            return Profile.NOT_FROM_ANDROID_SAMPLE;
        } else {
            String[] nameFields = fields[1].split(":");
            return nameFields[1];
        }
    }

}
