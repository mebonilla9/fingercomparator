package com.griaulebiometrics.mobile.util;

public class GrConstants {

    //Intents handled
    public static final String USB_ATTACHED = new String("android.hardware.usb.action.USB_DEVICE_ATTACHED");
    public static final String USB_DETACHED = new String("android.hardware.usb.action.USB_DEVICE_DETACHED");
    public static final String START_SERVICE = new String("com.griaulebiometrics.mobile.connect");

    //Messages
    public static final int IMAGE_ACQURIED = 1;

    //Vendor-ids
    public static final int VENDOR_FUTRONIC = 5265;
    public static final int VENDOR_DIGITAL_PERSONA = 1466;

    //Product ids
    public static final int PRODUCT_FS80 = 32;
    public static final int PRODUCT_FS88 = 136;
    public static final int PRODUCT_UAREU4500 = 10;

}
