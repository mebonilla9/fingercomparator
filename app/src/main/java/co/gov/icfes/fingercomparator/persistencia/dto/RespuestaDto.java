package co.gov.icfes.fingercomparator.persistencia.dto;

/**
 * Created by Lord_Nightmare on 9/07/15.
 */
public class RespuestaDto {

    private int codigo;
    private String mensaje;
    private Object datos;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getDatos() {
        return datos;
    }

    public void setDatos(Object datos) {
        this.datos = datos;
    }
}
