package co.gov.icfes.fingercomparator.actividades;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.griaulebiometrics.mobile.NativeService;
import com.griaulebiometrics.mobile.Template;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.util.Calendar;

import co.gov.icfes.fingercomparator.R;
import co.gov.icfes.fingercomparator.negocio.util.AlertGenerator;
import co.gov.icfes.fingercomparator.negocio.util.SelectorHuella;
import co.gov.icfes.fingercomparator.persistencia.dao.ExamenesDao;
import co.gov.icfes.fingercomparator.persistencia.entidades.Examenes;


public class NuevoExamenActivity extends AppCompatActivity {


    private BootstrapEditText txtSalon;
    private BootstrapEditText txtRegistro;
    private BootstrapEditText txtCitaSnee;
    private BootstrapEditText txtNombre;
    private BootstrapEditText txtTipoDocumento;
    private BootstrapEditText txtDocumento;
    private BootstrapEditText txtOrdenSalon;
    private BootstrapEditText txtFormaCuad;
    private BootstrapEditText txtNumCuad;
    private BootstrapButton btnGetBarcode;
    private BootstrapButton btnFotoCedula;
    private BootstrapButton btnFotoHuellaCedula;
    private BootstrapButton btnRegistrarExamen;

    private SelectorHuella selector;

    //captured picture uri
    private Uri picUri;
    private String fotoHdUrl;
    private byte[] fotoImagen;

    private final int CAMERA_CAPTURE = 1;
    private final int PIC_CROP = 2;
    private static final int GET_HUELLA = 3;
    private static final int GET_BARCODE = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_examen);

        this.btnRegistrarExamen = (BootstrapButton) findViewById(R.id.btnRegistrarExamen);
        this.btnFotoHuellaCedula = (BootstrapButton) findViewById(R.id.btnFotoHuellaCedula);
        this.btnFotoCedula = (BootstrapButton) findViewById(R.id.btnFotoCedula);
        this.btnGetBarcode = (BootstrapButton) findViewById(R.id.btnGetBarcode);

        this.txtNumCuad = (BootstrapEditText) findViewById(R.id.txtNumCuad);
        this.txtFormaCuad = (BootstrapEditText) findViewById(R.id.txtFormaCuad);
        this.txtOrdenSalon = (BootstrapEditText) findViewById(R.id.txtOrdenSalon);
        this.txtDocumento = (BootstrapEditText) findViewById(R.id.txtDocumento);
        this.txtTipoDocumento = (BootstrapEditText) findViewById(R.id.txtTipoDocumento);
        this.txtNombre = (BootstrapEditText) findViewById(R.id.txtNombre);
        this.txtCitaSnee = (BootstrapEditText) findViewById(R.id.txtCitaSnee);
        this.txtRegistro = (BootstrapEditText) findViewById(R.id.txtRegistro);
        this.txtSalon = (BootstrapEditText) findViewById(R.id.txtSalon);

        selector = new SelectorHuella();

        asignarEventos();
    }

    private void asignarEventos() {
        btnFotoHuellaCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selector.getColeccionHuellas().size() < 3) {
                    startActivityForResult(new Intent(NuevoExamenActivity.this, FingerActivity.class), GET_HUELLA);
                } else {
                    Toast.makeText(NuevoExamenActivity.this, "Han sido tomadas todas las huellas", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnGetBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(NuevoExamenActivity.this);
                scanIntegrator.initiateScan();
            }
        });
        btnFotoCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //we will handle the returned data in onActivityResult
                startActivityForResult(captureIntent, CAMERA_CAPTURE);


            }
        });
        btnRegistrarExamen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (selector.getColeccionHuellas().size()>=3 && fotoHdUrl.contains("file://")) {
                        Examenes examen = new Examenes();
                        String[] dataPreferencias = getDataPreferencias();
                        examen.setCodigoSitio(Long.parseLong(dataPreferencias[0]));
                        examen.setNombreSitio(dataPreferencias[1]);
                        examen.setSalon(txtSalon.getText().toString());
                        examen.setRegistro(Long.parseLong(txtRegistro.getText().toString()));
                        examen.setCitaSnee(txtCitaSnee.getText().toString());
                        examen.setNombre(txtNombre.getText().toString());
                        examen.setTipoDoc(txtTipoDocumento.getText().toString());
                        examen.setDocumento(Long.parseLong(txtDocumento.getText().toString()));
                        examen.setOrdenSalon(Long.parseLong(txtDocumento.getText().toString()));
                        examen.setFormCuad(txtFormaCuad.getText().toString());
                        examen.setNumCuad(Long.parseLong(txtNumCuad.getText().toString()));
                        examen.setRutaImagen1(saveImage(templateToImage(selector.getColeccionHuellas().get(0).getBuffer()), examen.getDocumento().toString(), 0));
                        examen.setRutaImagen2(saveImage(templateToImage(selector.getColeccionHuellas().get(1).getBuffer()), examen.getDocumento().toString(), 1));
                        examen.setRutaImagen3(saveImage(templateToImage(selector.getColeccionHuellas().get(2).getBuffer()), examen.getDocumento().toString(), 2));
                        examen.setRutaImagenHd(fotoHdUrl);
                        examen.setTemplate(selector.retornarMaxCalidad());
                        Calendar calendar = Calendar.getInstance();
                        examen.setFechaExamen(new Date(calendar.getTimeInMillis()));

                        NativeService ns = NativeService.getInstance();
                        byte [] fotoTemp = ns.extraerTemplate(fotoImagen);
                        int score = ns.compararHuellasExaminado(selector.retornarMaxCalidad(),fotoTemp);
                        if(score>NativeService.MATCH_THREASHOLD){
                            new ExamenesDao(NuevoExamenActivity.this).insertar(examen);
                            AlertGenerator.mostrarAlerta(
                                "Registro de Examen",
                                "La información suministrada coincide con el evaluado\nRegistro de exámen completado",
                                NuevoExamenActivity.this,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(NuevoExamenActivity.this, "Examen registrado", Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(NuevoExamenActivity.this, HomeActivity.class));
                                    }
                                }, null
                            );
                        }
                        else{
                            AlertGenerator.mostrarAlerta(
                                    "Registro de Examen",
                                    "La huella digital del evaluado y su cedula no coinciden\nNo es posible registrar el examen",
                                    NuevoExamenActivity.this,
                                    null, null
                            );
                        }
                    }
                    else{
                        AlertGenerator.mostrarAlerta(
                                "Registro de autorizado",
                                "Debe ingresar 3 huellas digitales y la foto de una huella digital para el procesamiento de registro",
                                NuevoExamenActivity.this,
                                null,
                                null
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult != null) {
                String scanContent = scanResult.getContents();
            } else {
                Toast.makeText(NuevoExamenActivity.this, "Datos no obtenidos del lector", Toast.LENGTH_SHORT).show();
            }
            if (requestCode == GET_HUELLA && resultCode == RESULT_OK) {
                NativeService ns = NativeService.getInstance();
                byte[] fingerData = data.getByteArrayExtra("captureFinger");
                Template template = ns.extractTemplate(fingerData);
                selector.agregarHuella(template);
                String rest = 3 - selector.getColeccionHuellas().size() + "";
                AlertGenerator.mostrarAlerta(
                        "Obtencion de Huella",
                        "Huella guardada, necesita " + rest + " para poder realizar el registro",
                        NuevoExamenActivity.this,
                        null,
                        null
                );
            }
            if (requestCode == CAMERA_CAPTURE) {
                picUri = data.getData();
                performCrop();
            }
            if (requestCode == PIC_CROP) {
                Bundle extras = data.getExtras();

                Bitmap scaledphoto = null;
                int height = 480;
                int width = 480;

                //get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");

                scaledphoto = Bitmap.createScaledBitmap(thePic, height, width, true);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                scaledphoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                fotoImagen = stream.toByteArray();

                fotoHdUrl = this.saveImage(scaledphoto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap templateToImage(byte[] buffer) {
        return BitmapFactory.decodeByteArray(buffer.clone(), 0, buffer.length);
    }

    private String saveImage(Bitmap bitmap) {
        try {
            int fotoId = this.getFotoSequence();
            OutputStream fOutputStream = null;
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/co.gov.icfes.fingercomparator/foto_" + fotoId + ".jpg");
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            fotoId++;
            this.saveCountFoto(fotoId);
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private String saveImage(Bitmap bitmap, String documento, int flag) {
        try {
            OutputStream fOutputStream = null;
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/co.gov.icfes.fingercomparator/" + documento + "/foto" + flag + ".jpg");
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void performCrop() {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            //cropIntent.putExtra("outputX", 256);
            //cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuevo_examen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String[] getDataPreferencias() {
        SharedPreferences preferencias = getSharedPreferences("icfesPreferencias", Context.MODE_PRIVATE);
        String[] data = new String[2];
        data[0] = preferencias.getString("codigo_sitio", "");
        data[1] = preferencias.getString("nombre_sitio", "");
        return data;
    }

    private int getFotoSequence() {
        SharedPreferences preferencias = getSharedPreferences("icfesPreferencias", Context.MODE_PRIVATE);
        return preferencias.getInt("count_foto", 0);
    }

    private void saveCountFoto(int valor) {
        SharedPreferences preferencias = getSharedPreferences("icfesPreferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putInt("count_foto", valor);
        editor.commit();
    }
}
