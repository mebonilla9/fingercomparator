package co.gov.icfes.fingercomparator.persistencia.conexion;

import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import co.gov.icfes.fingercomparator.negocio.constantes.EMensajes;
import co.gov.icfes.fingercomparator.negocio.excepciones.FingerComparatorException;

/**
 * Created by Lord_Nightmare on 25/05/15.
 */

public class BdConexion {

    private static final String DATABASE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/co.gov.icfes.fingercomparator";

    public static SQLiteDatabase conectar(String androidId) throws FingerComparatorException {
        try {
            return SQLiteDatabase.openDatabase(DATABASE_DIR + "/icfes" + androidId + ".sqlite", null, 0);
        } catch (Exception e) {
            throw new FingerComparatorException(EMensajes.ERROR_CONEXION_BD);
        }
    }

    public static void desconectar(SQLiteDatabase db){
        db.close();
    }

}
