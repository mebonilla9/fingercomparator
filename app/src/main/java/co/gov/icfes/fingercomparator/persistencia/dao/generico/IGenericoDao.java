package co.gov.icfes.fingercomparator.persistencia.dao.generico;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Lord_Nightmare on 24/06/15.
 */

public interface IGenericoDao<T> {

    void insertar(T entidad) throws SQLException;

    void editar(T entidad) throws SQLException;

    List<T> consultar() throws SQLException;

    T consultar(Long id) throws SQLException;
}
