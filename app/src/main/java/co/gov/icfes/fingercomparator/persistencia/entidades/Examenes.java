package co.gov.icfes.fingercomparator.persistencia.entidades;

import java.sql.Date;

/**
 * Created by Lord_Nightmare on 25/05/15.
 */
public class Examenes {

    private Long idExamen;
    private Long codigoSitio;
    private String nombreSitio;
    private String salon;
    private Long registro;
    private String citaSnee;
    private String nombre;
    private String tipoDoc;
    private Long documento;
    private Long ordenSalon;
    private String formCuad;
    private Long numCuad;
    private String rutaImagen1;
    private String rutaImagen2;
    private String rutaImagen3;
    private String rutaImagenHd;
    private byte[] template;
    private Date fechaExamen;


    public Long getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(Long idExamen) {
        this.idExamen = idExamen;
    }

    public Long getCodigoSitio() {
        return codigoSitio;
    }

    public void setCodigoSitio(Long codigoSitio) {
        this.codigoSitio = codigoSitio;
    }

    public String getNombreSitio() {
        return nombreSitio;
    }

    public void setNombreSitio(String nombreSitio) {
        this.nombreSitio = nombreSitio;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public Long getRegistro() {
        return registro;
    }

    public void setRegistro(Long registro) {
        this.registro = registro;
    }

    public String getCitaSnee() {
        return citaSnee;
    }

    public void setCitaSnee(String citaSnee) {
        this.citaSnee = citaSnee;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public Long getDocumento() {
        return documento;
    }

    public void setDocumento(Long documento) {
        this.documento = documento;
    }

    public Long getOrdenSalon() {
        return ordenSalon;
    }

    public void setOrdenSalon(Long ordenSalon) {
        this.ordenSalon = ordenSalon;
    }

    public String getFormCuad() {
        return formCuad;
    }

    public void setFormCuad(String formCuad) {
        this.formCuad = formCuad;
    }

    public Long getNumCuad() {
        return numCuad;
    }

    public void setNumCuad(Long numCuad) {
        this.numCuad = numCuad;
    }

    public String getRutaImagen1() {
        return rutaImagen1;
    }

    public void setRutaImagen1(String rutaImagen1) {
        this.rutaImagen1 = rutaImagen1;
    }

    public String getRutaImagen2() {
        return rutaImagen2;
    }

    public void setRutaImagen2(String rutaImagen2) {
        this.rutaImagen2 = rutaImagen2;
    }

    public String getRutaImagen3() {
        return rutaImagen3;
    }

    public void setRutaImagen3(String rutaImagen3) {
        this.rutaImagen3 = rutaImagen3;
    }

    public String getRutaImagenHd() {
        return rutaImagenHd;
    }

    public void setRutaImagenHd(String rutaImagenHd) {
        this.rutaImagenHd = rutaImagenHd;
    }

    public byte[] getTemplate() {
        return template;
    }

    public void setTemplate(byte[] template) {
        this.template = template;
    }

    public Date getFechaExamen() {
        return fechaExamen;
    }

    public void setFechaExamen(Date fechaExamen) {
        this.fechaExamen = fechaExamen;
    }
}
