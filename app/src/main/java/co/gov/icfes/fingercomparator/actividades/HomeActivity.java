package co.gov.icfes.fingercomparator.actividades;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.beardedhen.androidbootstrap.BootstrapButton;

import co.gov.icfes.fingercomparator.R;
import co.gov.icfes.fingercomparator.negocio.util.AlertGenerator;


public class HomeActivity extends AppCompatActivity {

    private android.widget.ImageView imageView;
    private com.beardedhen.androidbootstrap.BootstrapButton btnNuevoExamen;
    private com.beardedhen.androidbootstrap.BootstrapButton btnCerrarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.btnCerrarSesion = (BootstrapButton) findViewById(R.id.btnCerrarSesion);
        this.btnNuevoExamen = (BootstrapButton) findViewById(R.id.btnNuevoExamen);
        this.imageView = (ImageView) findViewById(R.id.imageView);
        asignarEventos();

    }

    private void asignarEventos() {
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertGenerator.mostrarAlerta("Cerrar Sesión", "Esta seguro de abandonar la sesión", HomeActivity.this,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(HomeActivity.this, MainActivity.class));
                            }
                        }, null
                );
            }
        });

        btnNuevoExamen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, NuevoExamenActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.nuevo_autorizado) {
            startActivity(new Intent(HomeActivity.this,AutorizacionActivity.class));
            return true;
        }
        if(id == R.id.datosExamenAuth){
            startActivity(new Intent(HomeActivity.this,DatosExamenActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
