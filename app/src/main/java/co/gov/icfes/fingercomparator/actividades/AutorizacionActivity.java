package co.gov.icfes.fingercomparator.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import co.gov.icfes.fingercomparator.R;


public class AutorizacionActivity extends AppCompatActivity {

    private final String ADMIN_PASS = "no me la acuerdo";

    private BootstrapEditText txtAdminPassword;
    private BootstrapButton btnCancelar;
    private BootstrapButton btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorizacion);
        this.btnIngresar = (BootstrapButton) findViewById(R.id.btnIngresar);
        this.btnCancelar = (BootstrapButton) findViewById(R.id.btnCancelar);
        this.txtAdminPassword = (BootstrapEditText) findViewById(R.id.txtAdminPassword);

        asignarEventos();
    }

    private void asignarEventos() {
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contraseña = txtAdminPassword.getText().toString();
                if(contraseña.equals(ADMIN_PASS)){
                    startActivity(new Intent(AutorizacionActivity.this,RegistrarActivity.class));
                    finish();
                }
                else{
                    txtAdminPassword.setState("danger");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_autorizacion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
