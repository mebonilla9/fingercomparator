package co.gov.icfes.fingercomparator.persistencia.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import co.gov.icfes.fingercomparator.negocio.excepciones.FingerComparatorException;
import co.gov.icfes.fingercomparator.persistencia.conexion.BdConexion;
import co.gov.icfes.fingercomparator.persistencia.dao.generico.IGenericoDao;
import co.gov.icfes.fingercomparator.persistencia.entidades.Examenes;

/**
 * Created by Lord_Nightmare on 23/06/15.
 */
public class ExamenesDao implements IGenericoDao<Examenes> {

    private final int ID = 1;
    private SQLiteDatabase db;
    private String androidId;

    public ExamenesDao(Context context) {
        this.androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);;
        try {
            this.db = BdConexion.conectar(this.androidId);
        } catch (FingerComparatorException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertar(Examenes entidad) throws SQLException {
        ContentValues entidadValues = new ContentValues();
        try {
            entidadValues.put("codigo_sitio", entidad.getCodigoSitio());
            entidadValues.put("nombre_sitio", entidad.getNombreSitio());
            entidadValues.put("salon", entidad.getSalon());
            entidadValues.put("registro", entidad.getRegistro());
            entidadValues.put("cita_snee", entidad.getCitaSnee());
            entidadValues.put("nombre", entidad.getNombre());
            entidadValues.put("tipo_doc", entidad.getTipoDoc());
            entidadValues.put("documento", entidad.getDocumento());
            entidadValues.put("form_cuad", entidad.getFormCuad());
            entidadValues.put("num_cuad", entidad.getNumCuad());
            entidadValues.put("orden_salon", entidad.getOrdenSalon());
            entidadValues.put("ruta_imagen_1", entidad.getRutaImagen1());
            entidadValues.put("ruta_imagen_2", entidad.getRutaImagen2());
            entidadValues.put("ruta_imagen_3", entidad.getRutaImagen3());
            entidadValues.put("ruta_imagen_hd", entidad.getRutaImagenHd());
            entidadValues.put("template", entidad.getTemplate());
            entidadValues.put("fecha_examen", entidad.getFechaExamen().toString());
            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            db.insert("examenes", null, entidadValues);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void editar(Examenes entidad) throws SQLException {
        ContentValues entidadValues = new ContentValues();
        try {
            entidadValues.put("id_examen", entidad.getIdExamen());
            entidadValues.put("codigo_sitio", entidad.getCodigoSitio());
            entidadValues.put("nombre_sitio", entidad.getNombreSitio());
            entidadValues.put("salon", entidad.getSalon());
            entidadValues.put("registro", entidad.getRegistro());
            entidadValues.put("cita_snee", entidad.getCitaSnee());
            entidadValues.put("nombre", entidad.getNombre());
            entidadValues.put("tipo_doc", entidad.getTipoDoc());
            entidadValues.put("documento", entidad.getDocumento());
            entidadValues.put("orden_salon", entidad.getOrdenSalon());
            entidadValues.put("form_cuad", entidad.getFormCuad());
            entidadValues.put("num_cuad", entidad.getNumCuad());
            entidadValues.put("ruta_imagen_1", entidad.getRutaImagen1());
            entidadValues.put("ruta_imagen_2", entidad.getRutaImagen2());
            entidadValues.put("ruta_imagen_3", entidad.getRutaImagen3());
            entidadValues.put("ruta_imagen_hd", entidad.getRutaImagenHd());
            entidadValues.put("template", entidad.getTemplate());
            entidadValues.put("fecha_examen", entidad.getFechaExamen().toString());

            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            db.update("examenes", entidadValues, "id_examen=?", new String[]{Long.toString(entidad.getIdExamen())});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public List<Examenes> consultar() throws SQLException {
        List<Examenes> listaExamenes = new ArrayList<>();
        try {
            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            Cursor cursor = db.query("examenes", null, null, null, null, null, null);
            cursor.moveToFirst();
            if (cursor.moveToFirst()) {
                do {
                    Examenes examen = new Examenes();
                    examen.setIdExamen(cursor.getLong(cursor.getColumnIndex("id_examen")));
                    examen.setCodigoSitio(cursor.getLong(cursor.getColumnIndex("codigo_sitio")));
                    examen.setNombre(cursor.getString(cursor.getColumnIndex("nombre_sitio")));
                    examen.setSalon(cursor.getString(cursor.getColumnIndex("salon")));
                    examen.setRegistro(cursor.getLong(cursor.getColumnIndex("registro")));
                    examen.setCitaSnee(cursor.getString(cursor.getColumnIndex("cita_snee")));
                    examen.setNombre(cursor.getString(cursor.getColumnIndex("nombre")));
                    examen.setTipoDoc(cursor.getString(cursor.getColumnIndex("tipo_doc")));
                    examen.setDocumento(cursor.getLong(cursor.getColumnIndex("documento")));
                    examen.setOrdenSalon(cursor.getLong(cursor.getColumnIndex("orden_salon")));
                    examen.setFormCuad(cursor.getString(cursor.getColumnIndex("form_cuad")));
                    examen.setNumCuad(cursor.getLong(cursor.getColumnIndex("num_cuad")));
                    examen.setRutaImagen1(cursor.getString(cursor.getColumnIndex("ruta_imagen_1")));
                    examen.setRutaImagen2(cursor.getString(cursor.getColumnIndex("ruta_imagen_2")));
                    examen.setRutaImagen3(cursor.getString(cursor.getColumnIndex("ruta_imagen_3")));
                    examen.setRutaImagenHd(cursor.getString(cursor.getColumnIndex("ruta_imagen_hd")));
                    examen.setTemplate(cursor.getBlob(cursor.getColumnIndex("template")));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    java.util.Date date = sdf.parse(cursor.getString(cursor.getColumnIndex("fecha_examen")));
                    examen.setFechaExamen(new Date(date.getTime()));
                    listaExamenes.add(examen);
                } while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();
            return listaExamenes;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return null;
    }

    @Override
    public Examenes consultar(Long id) throws SQLException {
        Examenes examen = null;
        try {
            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            Cursor cursor = db.query("examenes", null, "id_examen=?", new String[]{Long.toString(id)}, null, null, null);
            cursor.moveToFirst();
            if (cursor.moveToFirst()) {
                do {
                    examen = new Examenes();
                    examen.setIdExamen(cursor.getLong(cursor.getColumnIndex("id_examen")));
                    examen.setCodigoSitio(cursor.getLong(cursor.getColumnIndex("codigo_sitio")));
                    examen.setNombre(cursor.getString(cursor.getColumnIndex("nombre_sitio")));
                    examen.setSalon(cursor.getString(cursor.getColumnIndex("salon")));
                    examen.setRegistro(cursor.getLong(cursor.getColumnIndex("registro")));
                    examen.setCitaSnee(cursor.getString(cursor.getColumnIndex("cita_snee")));
                    examen.setNombre(cursor.getString(cursor.getColumnIndex("nombre")));
                    examen.setTipoDoc(cursor.getString(cursor.getColumnIndex("tipo_doc")));
                    examen.setDocumento(cursor.getLong(cursor.getColumnIndex("documento")));
                    examen.setOrdenSalon(cursor.getLong(cursor.getColumnIndex("orden_salon")));
                    examen.setFormCuad(cursor.getString(cursor.getColumnIndex("form_cuad")));
                    examen.setNumCuad(cursor.getLong(cursor.getColumnIndex("num_cuad")));
                    examen.setRutaImagen1(cursor.getString(cursor.getColumnIndex("ruta_imagen_1")));
                    examen.setRutaImagen2(cursor.getString(cursor.getColumnIndex("ruta_imagen_2")));
                    examen.setRutaImagen3(cursor.getString(cursor.getColumnIndex("ruta_imagen_3")));
                    examen.setRutaImagenHd(cursor.getString(cursor.getColumnIndex("ruta_imagen_hd")));
                    examen.setTemplate(cursor.getBlob(cursor.getColumnIndex("template")));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    java.util.Date date = sdf.parse(cursor.getString(cursor.getColumnIndex("fecha_examen")));
                    examen.setFechaExamen(new Date(date.getTime()));
                } while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();
            return examen;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return null;
    }
}
