package co.gov.icfes.fingercomparator.negocio.excepciones;

import co.gov.icfes.fingercomparator.negocio.constantes.EMensajes;

/**
 * Created by Lord_Nightmare on 25/05/15.
 */
public class FingerComparatorException extends Exception{

    private int codigo;
    private String mensaje;
    private Object datos;

    public FingerComparatorException(EMensajes mensaje) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
    }

    public FingerComparatorException(EMensajes mensaje, Object datos) {
        this.codigo = mensaje.getCodigo();
        this.mensaje = mensaje.getDescripcion();
        this.datos = datos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getDatos() {
        return datos;
    }

    public void setDatos(Object datos) {
        this.datos = datos;
    }

}
