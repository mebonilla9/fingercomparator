package co.gov.icfes.fingercomparator.negocio.util;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.List;

/**
 * Created by Lord_Nightmare on 24/06/15.
 */
public class PreviewCamera extends SurfaceView implements SurfaceHolder.Callback, Camera.PictureCallback {

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private RawCallback mRawCallback;

    public PreviewCamera(Context context) {
        super(context);

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mRawCallback = new RawCallback();
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(mRawCallback, mRawCallback, null, PreviewCamera.this);
            }
        });
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
        configure(mCamera);
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (Exception e) {
            closeCamara();
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        /*Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPictureSize(width, height);
        mCamera.setParameters(parameters);*/
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        closeCamara();
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        mCamera.startPreview();
    }

    private void configure(Camera camera) {
        Camera.Parameters params = camera.getParameters();
        List<Integer> formats = params.getSupportedPictureFormats();
        if (formats.contains(PixelFormat.RGB_565)) {
            params.setPictureFormat(PixelFormat.RGB_565);
        } else {
            params.setPictureFormat(PixelFormat.JPEG);
        }
        //List<Camera.Size> sizes = params.getSupportedPictureSizes();
        //Camera.Size size = sizes.get(sizes.size() - 1);
        params.setPictureSize(320, 480);

        /*params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        List<String> sceneModes = params.getSupportedSceneModes();
        if (sceneModes.contains(Camera.Parameters.SCENE_MODE_ACTION)) {
            params.setSceneMode(Camera.Parameters.SCENE_MODE_ACTION);
        } else {
            params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
        }*/
    }

    private void closeCamara(){
        if (mCamera != null){
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }
}
