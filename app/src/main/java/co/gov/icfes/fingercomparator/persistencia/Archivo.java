package co.gov.icfes.fingercomparator.persistencia;

import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Type;
import java.util.List;

import co.gov.icfes.fingercomparator.persistencia.entidades.Autorizados;
import co.gov.icfes.fingercomparator.persistencia.entidades.Examenes;

/**
 * Created by Lord_Nightmare on 10/07/15.
 */
public class Archivo {

    private final String RUTA_ARCHIVO = Environment.getExternalStorageDirectory()
            .getAbsolutePath()+"/listas";

    public Archivo(){
        try {
            File directorio = new File(RUTA_ARCHIVO);
            directorio.mkdirs();
            Log.v("Creacion de carpeta", "Carpeta creada");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void escritura(Examenes examenData){
        try {
            Gson gson = new Gson();
            String json = gson.toJson(examenData);
            PrintStream ps = new PrintStream(RUTA_ARCHIVO+"/personas_json.txt");
            ps.print(json);
            ps.flush();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Autorizados lectura(){
        try {

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(
                                    RUTA_ARCHIVO+"/personas_json.txt")
                    )
            );
            String contenido = "";
            while (br.ready()){
                contenido += br.readLine();
            }
            br.close();
            Type type = new TypeToken<Examenes>(){}.getType();
            return new Gson().fromJson(contenido,type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
