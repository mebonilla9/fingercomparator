package co.gov.icfes.fingercomparator.persistencia.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.gov.icfes.fingercomparator.negocio.excepciones.FingerComparatorException;
import co.gov.icfes.fingercomparator.persistencia.conexion.BdConexion;
import co.gov.icfes.fingercomparator.persistencia.dao.generico.IGenericoDao;
import co.gov.icfes.fingercomparator.persistencia.entidades.Autorizados;

/**
 * Created by Lord_Nightmare on 23/06/15.
 */
public class AutorizadosDao implements IGenericoDao<Autorizados>{

    private final int ID = 1;
    private SQLiteDatabase db;
    private String androidId;

    public AutorizadosDao(Context context) {
        this.androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        try {
            this.db = BdConexion.conectar(this.androidId);
        } catch (FingerComparatorException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertar(Autorizados entidad) throws SQLException {
        ContentValues autorizadosValues = new ContentValues();
        try {
            autorizadosValues.put("nombre", entidad.getNombre());
            autorizadosValues.put("template", entidad.getNombre());
            autorizadosValues.put("correo", entidad.getNombre());
            autorizadosValues.put("documento", entidad.getNombre());
            db.beginTransaction();
            db.insert("examenes", null, autorizadosValues);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void editar(Autorizados entidad) throws SQLException {
        ContentValues autorizadosValues = new ContentValues();
        try {
            autorizadosValues.put("id", entidad.getId());
            autorizadosValues.put("nombre", entidad.getNombre());
            autorizadosValues.put("template", entidad.getTemplate());
            autorizadosValues.put("correo", entidad.getCorreo());
            autorizadosValues.put("documento", entidad.getDocumento());
            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            db.update("autorizados", autorizadosValues, "id=?", new String[]{Long.toString(entidad.getId())});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public List<Autorizados> consultar() throws SQLException {
        List<Autorizados> listaAutorizados = new ArrayList<>();
        try {
            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            Cursor cursor = db.query("autorizados", null, null, null, null, null, null);
            cursor.moveToFirst();
            if (cursor.moveToFirst()) {
                do {
                    Autorizados autorizado = new Autorizados();
                    autorizado.setId(cursor.getLong(cursor.getColumnIndex("id")));
                    autorizado.setNombre(cursor.getString(cursor.getColumnIndex("nombre")));
                    autorizado.setTemplate(cursor.getBlob(cursor.getColumnIndex("template")));
                    autorizado.setCorreo(cursor.getString(cursor.getColumnIndex("correo ")));
                    autorizado.setDocumento(cursor.getLong(cursor.getColumnIndex("documento")));
                } while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();
            return listaAutorizados;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return null;
    }

    @Override
    public Autorizados consultar(Long id) throws SQLException {
        Autorizados autorizado = null;
        try {
            SQLiteDatabase db = BdConexion.conectar(this.androidId);
            db.beginTransaction();
            Cursor cursor = db.query("autorizados", null, "id=?", new String[]{Long.toString(id)}, null, null, null);
            cursor.moveToFirst();
            if (cursor.moveToFirst()) {
                do {
                    autorizado = new Autorizados();
                    autorizado.setId(cursor.getLong(cursor.getColumnIndex("id")));
                    autorizado.setNombre(cursor.getString(cursor.getColumnIndex("nombre")));
                    autorizado.setTemplate(cursor.getBlob(cursor.getColumnIndex("template")));
                    autorizado.setCorreo(cursor.getString(cursor.getColumnIndex("correo ")));
                    autorizado.setDocumento(cursor.getLong(cursor.getColumnIndex("documento")));
                } while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();
            return autorizado;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return null;
    }
}
