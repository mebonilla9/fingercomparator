package co.gov.icfes.fingercomparator.actividades;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.griaulebiometrics.mobile.NativeService;
import com.griaulebiometrics.mobile.Template;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import co.gov.icfes.fingercomparator.R;
import co.gov.icfes.fingercomparator.negocio.util.AlertGenerator;
import co.gov.icfes.fingercomparator.negocio.util.SelectorHuella;
import co.gov.icfes.fingercomparator.negocio.util.UtilidadDescargaHttps;
import co.gov.icfes.fingercomparator.persistencia.dao.AutorizadosDao;
import co.gov.icfes.fingercomparator.persistencia.dto.RespuestaDto;
import co.gov.icfes.fingercomparator.persistencia.entidades.Autorizados;


public class RegistrarActivity extends AppCompatActivity {

    private final int GET_HUELLA = 1;

    private BootstrapEditText txtNombre;
    private BootstrapEditText txtCorreo;
    private BootstrapEditText txtDocumento;
    private BootstrapButton btnObtenerHuella;
    private BootstrapButton btnRegistrarExamen;
    private SelectorHuella selector;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        this.btnRegistrarExamen = (BootstrapButton) findViewById(R.id.btnRegistrarExamen);
        this.btnObtenerHuella = (BootstrapButton) findViewById(R.id.btnObtenerHuella);

        this.txtDocumento = (BootstrapEditText) findViewById(R.id.txtDocumento);
        this.txtCorreo = (BootstrapEditText) findViewById(R.id.txtCorreo);
        this.txtNombre = (BootstrapEditText) findViewById(R.id.txtNombre);

        selector = new SelectorHuella();
        asignarEventos();

        asignarHandler();

    }

    private void asignarHandler() {
        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                if(message.obj.toString().contains("codigo")){
                    Type tipo = new TypeToken<RespuestaDto>(){}.getType();
                    RespuestaDto resdto = new Gson().fromJson(message.obj.toString(), tipo);
                    if (resdto.getCodigo()==1){
                        AlertGenerator.mostrarAlerta(
                                "Registro de autorizado",
                                "Se ha regostrado un nuevo Autorizado",
                                RegistrarActivity.this,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        startActivity(new Intent(RegistrarActivity.this,HomeActivity.class));
                                    }
                                },null
                        );
                    }
                }
                return false;
            }
        });
    }

    private void asignarEventos() {
        btnObtenerHuella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selector.getColeccionHuellas().size() < 3) {
                    startActivityForResult(new Intent(RegistrarActivity.this, FingerActivity.class), GET_HUELLA);
                } else {
                    Toast.makeText(RegistrarActivity.this, "Han sido tomadas todas las huellas", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnRegistrarExamen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (selector.getColeccionHuellas().size() >= 3) {
                        Autorizados autorizado = new Autorizados();
                        autorizado.setNombre(txtNombre.getText().toString());
                        autorizado.setDocumento(Long.parseLong(txtDocumento.toString()));
                        autorizado.setCorreo(txtCorreo.getText().toString());
                        autorizado.setTemplate(selector.retornarMaxCalidad());
                        enviarNuevoAutorizado(autorizado);
                        AlertGenerator.mostrarAlerta("Registro de Autorizado",
                                "Se ha registrado un nuevo Autorizado",
                                RegistrarActivity.this,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        startActivity(new Intent(RegistrarActivity.this,MainActivity.class));
                                    }
                                },null);
                    }
                    else{
                        AlertGenerator.mostrarAlerta(
                                "Registro de autorizado",
                                "Debe ingresar 3 huellas digitales para el procesamiento de enrolamiento",
                                RegistrarActivity.this,
                                null,
                                null
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void enviarNuevoAutorizado(final Autorizados autorizado) {
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //UtilidadDescargaHttps.ignoreAllCerts();
                    String urlServidor = "http://middlebrain.net:80/FingerService/autorizados/insertar";
                    URL url = new URL(urlServidor);
                    //HttpsURLConnection conexion = (HttpsURLConnection) url.openConnection()
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setRequestMethod("POST");
                    OutputStreamWriter out=
                            new OutputStreamWriter(
                                    conexion.getOutputStream()
                            );
                    out.write("nombre="+autorizado.getNombre());
                    out.write("&correo="+autorizado.getCorreo());
                    out.write("&documento="+autorizado.getCorreo());
                    out.write("&template="+ Base64.encodeToString(autorizado.getTemplate(), 0));
                    out.flush();
                    out.close();
                    InputStreamReader canal = new InputStreamReader(conexion
                            .getInputStream());
                    BufferedReader lectura = new BufferedReader(canal);
                    String linea = lectura.readLine();
                    String respuesta = "";
                    while (linea != null) {
                        respuesta += linea;
                        linea = lectura.readLine();
                    }
                    lectura.close();
                    conexion.disconnect();
                    Message mensaje = new Message();
                    mensaje.obj = respuesta;
                    handler.sendMessage(mensaje);
                } catch (Exception e) {
                    e.printStackTrace();
                    Message mensaje = new Message();
                    mensaje.obj = "Error de conexión";
                    handler.sendMessage(mensaje);
                }
            }
        });
        hilo.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_HUELLA && resultCode == RESULT_OK) {
            NativeService ns = NativeService.getInstance();
            byte[] fingerData = data.getByteArrayExtra("captureFinger");
            Template template = ns.extractTemplate(fingerData);
            selector.agregarHuella(template);
            String rest = 3 - selector.getColeccionHuellas().size() + "";
            AlertGenerator.mostrarAlerta(
                    "Obtencion de Huella",
                    "Huella guardada, necesita " + rest + " para poder realizar el registro",
                    RegistrarActivity.this,
                    null,
                    null
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registrar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
