package co.gov.icfes.fingercomparator.actividades;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import co.gov.icfes.fingercomparator.R;
import co.gov.icfes.fingercomparator.negocio.util.AlertGenerator;


public class DatosExamenActivity extends AppCompatActivity {

    private com.beardedhen.androidbootstrap.BootstrapEditText txtCodigoSitio;
    private com.beardedhen.androidbootstrap.BootstrapEditText txtNombreSitio;
    private com.beardedhen.androidbootstrap.BootstrapButton btnCancelarDatos;
    private com.beardedhen.androidbootstrap.BootstrapButton btnIngresarDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_examen);
        this.btnIngresarDatos = (BootstrapButton) findViewById(R.id.btnIngresarDatos);
        this.btnCancelarDatos = (BootstrapButton) findViewById(R.id.btnCancelarDatos);
        this.txtNombreSitio = (BootstrapEditText) findViewById(R.id.txtNombreSitio);
        this.txtCodigoSitio = (BootstrapEditText) findViewById(R.id.txtCodigoSitio);

        asignarEventos();
    }

    private void asignarEventos() {
        btnCancelarDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnIngresarDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int registrable = 1;
                String nombreSitio = txtNombreSitio.getText().toString().trim();
                String codigoSitio = txtCodigoSitio.getText().toString().trim();
                if(codigoSitio.equals("")){
                    registrable = 0;
                    txtCodigoSitio.setState("danger");
                }
                if(nombreSitio.equals("")){
                    registrable = 0;
                    txtNombreSitio.setState("danger");
                }
                if(registrable>0){
                    SharedPreferences preferencias = getSharedPreferences("icfesPreferencias",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferencias.edit();
                    editor.putString("codigo_sitio",codigoSitio);
                    editor.putString("nombre_sitio",nombreSitio);
                    editor.commit();
                    AlertGenerator.mostrarAlerta(
                        "Registro de sitio",
                        "La informacion del sitio del examen ha sido registrada",
                        DatosExamenActivity.this,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        },null
                    );
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_datos_examen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
