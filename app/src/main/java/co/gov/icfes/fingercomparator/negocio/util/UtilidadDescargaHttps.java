package co.gov.icfes.fingercomparator.negocio.util;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Lord_Nightmare on 23/06/15.
 */
public class UtilidadDescargaHttps {

    private static final int BUFFER_SIZE = 4096;
    private static final String TAG = "UtilidadDescargaHttps";

    public static File descargarArchivo(String urlArchivo, String dirGuardar) {
        try {
            File downloadFile = null;
            ignoreAllCerts();
            URL url = new URL(urlArchivo);
            //HttpsURLConnection httpsConn = (HttpsURLConnection) url.openConnection();
            HttpURLConnection httpsConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpsConn.getResponseCode();
            //if (responseCode == HttpsURLConnection.HTTP_OK) {
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String nombreArchivo = "";
                String disposicion = httpsConn.getHeaderField("Content-Disposition");
                String tipoContenido = httpsConn.getContentType();
                int longitudContenido = httpsConn.getContentLength();

                if (disposicion != null) {
                    int indice = disposicion.indexOf("filename=");
                    if (indice > 0) {
                        nombreArchivo = disposicion.substring(indice + 10, disposicion.length() - 1);
                    }
                } else {
                    nombreArchivo = urlArchivo.substring(urlArchivo.lastIndexOf("/") + 1, urlArchivo.length());
                }

                Log.e(TAG, "Content-Type = " + tipoContenido);
                Log.e(TAG, "Content-Disposition = " + disposicion);
                Log.e(TAG, "Content-Length = " + longitudContenido);
                Log.e(TAG, "fileName = " + nombreArchivo);

                InputStream inputStream = httpsConn.getInputStream();
                String rutaArchivoGuardar = dirGuardar + File.separator + nombreArchivo;

                FileOutputStream outputStream = new FileOutputStream(rutaArchivoGuardar);

                int bytesLeidos = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesLeidos = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesLeidos);
                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();
                downloadFile = new File(rutaArchivoGuardar);

                Log.e(TAG, "Archivo descargado");
            } else {
                Log.e(TAG, "Archivo no descargado. Codigo respuesta servidor: " + responseCode);
            }
            httpsConn.disconnect();
            if (downloadFile.exists()) {
                return downloadFile;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void ignoreAllCerts() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    Log.i("RestUtilImpl", "Approving certificate for " + hostname);
                    return true;
                }
            });
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
