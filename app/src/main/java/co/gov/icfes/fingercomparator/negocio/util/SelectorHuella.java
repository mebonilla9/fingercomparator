package co.gov.icfes.fingercomparator.negocio.util;

import com.griaulebiometrics.mobile.Template;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lord_Nightmare on 9/07/15.
 */
public class SelectorHuella {

    private final String HUELLA_AGREGADA = "Huella agregada";
    private final String LIMITE_HUELLA = "No es posible agregar mas huellas el limite maximo es de 3";

    private List<Template> coleccionHuellas;

    public SelectorHuella() {
        this.coleccionHuellas = new ArrayList<>();
    }

    public String agregarHuella(Template template) {
        try {
            if (getColeccionHuellas().size() < 4) {
                getColeccionHuellas().add(template);
                return HUELLA_AGREGADA;
            } else {
                return LIMITE_HUELLA;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Error agregando huella";
        }
    }

    public byte[] retornarMaxCalidad() {
        int max = 0;
        for (Template t : getColeccionHuellas()) {
            if (t.getQuality() > max) {
                max = t.getQuality();
            }
        }
        Template tem = obtenerTemplate(max);
        return tem.getBuffer();
    }

    public byte[] retornarMinCalidad() {
        int min = 100;
        for (Template t : getColeccionHuellas()) {
            if (t.getQuality() < min) {
                min = t.getQuality();
            }
        }
        Template tem = obtenerTemplate(min);
        return tem.getBuffer();
    }

    public Template obtenerTemplate(int calidad) {
        for (Template tem : getColeccionHuellas()) {
            if (tem.getQuality() == calidad) {
                return tem;
            }
        }
        return null;
    }

    public List<Template> getColeccionHuellas() {
        return coleccionHuellas;
    }

    public void setColeccionHuellas(List<Template> coleccionHuellas) {
        this.coleccionHuellas = coleccionHuellas;
    }
}
