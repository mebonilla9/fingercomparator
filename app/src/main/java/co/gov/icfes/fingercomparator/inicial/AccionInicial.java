package co.gov.icfes.fingercomparator.inicial;

import android.app.Application;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import co.gov.icfes.fingercomparator.negocio.util.UtilidadDescargaHttps;

/**
 * Created by Lord_Nightmare on 22/06/15.
 */
public class AccionInicial extends Application {

    private final String urlDatabase = Environment.getExternalStorageDirectory().getAbsolutePath() + "/co.gov.icfes.fingercomparator";

    @Override
    public void onCreate() {
        try {
            String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            Log.d("Application Context", "Id: " + androidId);
            File databaseFile = new File(urlDatabase + "/icfes" + androidId + ".sqlite");
            if (!databaseFile.exists()) {
                File dirIcfes = new File(urlDatabase);
                if (!dirIcfes.exists()) {
                    dirIcfes.mkdirs();
                }
                crearPeticion(androidId, urlDatabase);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void descomprimirBaseDatos(String urlDatabase, File zipFile) {
        byte[] buffer = new byte[1024];
        try {
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {
                String nombreArchivo = ze.getName();
                File nuevoArchivo = new File(urlDatabase + File.separator + nombreArchivo);
                Log.e("Url nuevo archivo", nuevoArchivo.getAbsolutePath());

                FileOutputStream fos = new FileOutputStream(nuevoArchivo);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.flush();
                fos.close();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            Log.e("nuevo archivo", "Descompresion completada");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    private void crearPeticion(final String devId, final String directorio) {
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String direccion = "http://middlebrain.net:80/FingerService/PruebaServlet?";
                    direccion += "idUsuario=" + devId;
                    File f = UtilidadDescargaHttps.descargarArchivo(direccion, directorio);
                    if (f != null && f.exists()) {
                        descomprimirBaseDatos(urlDatabase, f);
                    } else {
                        Log.e("nuevo archivo", "Archivo no encontrado ex");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        hilo.start();
    }
}
