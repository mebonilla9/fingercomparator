package co.gov.icfes.fingercomparator.negocio.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Lord_Nightmare on 29/06/15.
 */
public class AlertGenerator {

    public static void mostrarAlerta(String titulo,String texto,Context contexto, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder alerta = new AlertDialog.Builder(contexto);
        alerta.setTitle(titulo);
        alerta.setMessage(texto);
        alerta.setPositiveButton("Aceptar", positiveListener);
        alerta.setNegativeButton("Cancelar",null);
        alerta.show();
    }

}
