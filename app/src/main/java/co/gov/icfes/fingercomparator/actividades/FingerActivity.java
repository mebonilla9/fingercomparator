package co.gov.icfes.fingercomparator.actividades;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.Messenger;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.griaulebiometrics.mobile.sample.ImageListener;
import com.griaulebiometrics.mobile.sample.IncomingHandler;
import com.griaulebiometrics.mobile.sensor.FS80;
import com.griaulebiometrics.mobile.sensor.Sensor;
import com.griaulebiometrics.mobile.util.GrConstants;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;

import co.gov.icfes.fingercomparator.R;


public class FingerActivity extends AppCompatActivity implements ImageListener {

    private static String TAG = "FingerActivity";
    private static BroadcastReceiver receiver;

    private BootstrapButton btnOkFinger;
    private ImageView imgHuella;
    private Sensor sensor;
    private static Messenger localMessenger;
    private byte[] imageByteArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger);
        this.imgHuella = (ImageView) findViewById(R.id.imgHuella);
        this.btnOkFinger = (BootstrapButton) findViewById(R.id.btnOkFinger);
        this.iniciarReceiver();

        Log.d(TAG, "Registrando Receiver");
        registerReceiver(receiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));

        Log.d(TAG, "Creating local messenger");
        localMessenger = new Messenger(new IncomingHandler(this));

        if (sensor != null) {
            Log.d(TAG, "Re asociando el sensor con la instancia");
            sensor.setListener(this);
        }

        asignarEventos();
    }

    private void asignarEventos() {
        btnOkFinger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intencion = new Intent();
                intencion.putExtra("captureFinger", imageByteArray);
                setResult(Activity.RESULT_OK, intencion);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "ejecucion on resume");
        verificarUsbConectado(getIntent(), false);
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "ejecucion on pause");
        if (sensor != null) {
            Log.d(TAG, "Sensor cerrado");
            sensor.close();
            sensor = null;
            Toast.makeText(getApplicationContext(), "Fingerprint reader deshabilitado.", Toast.LENGTH_SHORT).show();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Ejecucion on destroy");
        if (receiver != null) {
            Log.d(TAG, "Eliminar registro receiver");
            unregisterReceiver(receiver);
        }
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e(TAG, "nuevo intent");
        verificarUsbConectado(intent, true);
    }


    private void iniciarReceiver() {
        if (receiver == null) {
            Log.d(TAG, "Creando BroadcastReceiver");
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (GrConstants.USB_DETACHED.equals(intent.getAction())) {
                        UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (usbDevice != null) {
                            Log.d(TAG, "usb Detached");
                            if (closeUsbConnection(usbDevice)) {
                                Toast.makeText(getApplicationContext(), "Lector de Huellas desconectado", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            };
        }
    }

    public boolean connectFingerprintReader(UsbDevice usbDevice) {
        try {
            if (sensor == null) {
                UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                PendingIntent permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent("com.android.example.USB_PERMISSION"), 0);

                switch (usbDevice.getVendorId()) {
                    case GrConstants.VENDOR_FUTRONIC:
                        switch (usbDevice.getProductId()) {
                            case GrConstants.PRODUCT_FS80:
                                usbManager.requestPermission(usbDevice, permissionIntent);
                                Log.d(TAG, "Lector Conectado FS80");
                                sensor = new FS80(usbManager, usbDevice, this);
                                sensor.start();
                                return true;
                            default:
                                return false;
                        }
                    default:
                        return false;
                }
            } else {
                Log.d(TAG, "El sensor ya esta conectado");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean closeUsbConnection(UsbDevice usbDevice) {
        if (sensor == null || !sensor.getDevice().getDeviceName().equals(usbDevice.getDeviceName())) {
            return false;
        }
        Log.d(TAG, "Sensor Cerrado");
        sensor.close();
        sensor = null;
        return true;
    }

    private void verificarUsbConectado(Intent intent, boolean attach) {
        UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        for (UsbDevice usbDevice : deviceList.values()) {
            if (connectFingerprintReader(usbDevice)) {
                Toast.makeText(getApplicationContext(), "Fingerprinter Reader" + (attach ? "attached" : "enabled") + ".", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_finger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void imageAcquired(byte[] bytes, Bitmap bitmap) {
        try {
            Message message = Message.obtain(null, GrConstants.IMAGE_ACQURIED);
            Bundle bundle = new Bundle();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            bundle.putByteArray("buffer", bytes.clone());
            bundle.putByteArray("image", stream.toByteArray());
            stream.close();
            message.setData(bundle);
            localMessenger.send(message);
            Log.e(TAG, new String(bytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setImage(byte[] buffer, byte[] image) {
        imageByteArray = buffer;
        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
        imgHuella.setImageBitmap(bmp);
        //saveImage(bmp);
    }

    private void saveImage(Bitmap bitmap){
        try {
            OutputStream fOutputStream = null;
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/imagen_1.jpg");
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
