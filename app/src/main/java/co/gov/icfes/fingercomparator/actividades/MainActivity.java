package co.gov.icfes.fingercomparator.actividades;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.griaulebiometrics.mobile.NativeService;

import java.io.OutputStream;
import java.util.List;

import co.gov.icfes.fingercomparator.R;
import co.gov.icfes.fingercomparator.negocio.util.AlertGenerator;
import co.gov.icfes.fingercomparator.persistencia.dao.AutorizadosDao;
import co.gov.icfes.fingercomparator.persistencia.entidades.Autorizados;


public class MainActivity extends AppCompatActivity {


    private BootstrapButton btnGetFinger;
    private android.widget.ImageView imageView;
    private BootstrapButton btnNewAutorizado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.btnNewAutorizado = (BootstrapButton) findViewById(R.id.btnNewAutorizado);
        this.imageView = (ImageView) findViewById(R.id.imageView);
        this.btnGetFinger = (BootstrapButton) findViewById(R.id.btnGetFinger);

        asignarEventos();
    }

    private void asignarEventos() {
        btnGetFinger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivityForResult(new Intent(MainActivity.this, FingerActivity.class), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnNewAutorizado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,AutorizacionActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (data.hasExtra("captureFinger")) {
                Log.d("huella capturada", "huella tomada");
                NativeService ns = NativeService.getInstance();
                byte[] template = ns.extraerTemplate(data.getByteArrayExtra("captureFinger"));
                List<Autorizados> listaAutorizados = new AutorizadosDao(MainActivity.this).consultar();
                Autorizados aut = ns.compararHuellaLogin(template,listaAutorizados);
                OutputStream outStream = null;
                if(aut != null){
                    AlertGenerator.mostrarAlerta(
                            "Inicio de sesión",
                            "Usuario Autenticado, Bienvenido...!",
                            MainActivity.this,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(MainActivity.this,HomeActivity.class));
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(MainActivity.this,HomeActivity.class));
                                }
                            }
                    );
                }
                else{
                    AlertGenerator.mostrarAlerta(
                        "Inicio de sesión",
                        "Huella digital no encontrada, no se encuentra autorizado para el registro de examenes",
                        MainActivity.this,
                        null,
                        null
                    );
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
